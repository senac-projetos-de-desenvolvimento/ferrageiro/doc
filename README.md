# Arquivo de documentação do projeto
### Ferrageiro - Soluções em venda online
Sistema de controle de ferragens e venda online.
- [Wiki do projeto](https://gitlab.com/projeto-de-desenvolvimento-2020.2/ferrageiro/doc/-/wikis/home): Todas informações do projeto.

- [Link da API](https://gitlab.com/projeto-de-desenvolvimento-2020.2/ferrageiro/api): API controla todo o sistema (Backend).

- [Link Ferrageiro](https://gitlab.com/projeto-de-desenvolvimento-2020.2/ferrageiro/admin): Sistema local de controle (Admin).

- [Link Ferragem Luz](https://gitlab.com/projeto-de-desenvolvimento-2020.2/ferrageiro/web): Sistema de vendas online (Loja).